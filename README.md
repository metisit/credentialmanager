# README #

This is a module that contains the Get-StoredCredentials cmdlet.
Use this cmdlet to securely store credentials used by your script.
The cmdlet has inline documentation. To use use it, start PowerShell en type:

 Import-Module CredentialManager.psm1

To get help, type:

 Get-Help Get-StoredCredential -Detailed

Thanks,

Theo Hardendood